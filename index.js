var express = require('express');


var port = process.env.PORT || 2004;
var app = express();
//*** Defining api router
var webchatRouter = express.Router();
app.use('/', webchatRouter);
app.use(express.static(__dirname));


app.listen(port, () => {
  console.log('listening on port %s', port);
});