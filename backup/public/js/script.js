'use strict';



$(function () {
    const socket = io();
    var recognition = null;
    //var baseurl = "https://npgp-techgarage-hrbot-fe.azurewebsites.net/bot";

    var baseurl = "http://localhost:5001/bot";
    var posted_msg = "";
    var _browserName = getBrowser();
    //const outputYou = document.querySelector('.output-you');
    //const outputBot = document.querySelector('.output-bot');

    const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    if(_browserName == "Chrome"){
        recognition = new SpeechRecognition();
        recognition.lang = 'en-US';
        recognition.interimResults = false;
        recognition.maxAlternatives = 1;

        document.querySelector('button').addEventListener('click', function(){
          recognition.start();
        });

        recognition.addEventListener('speechstart', function() {
          console.log('Speech has been detected.');
        });

        recognition.addEventListener('result', function(e) {
          console.log('Result has been detected.');

          let last = e.results.length - 1;
          let text = e.results[last][0].transcript;

          outputYou.textContent = text;
          console.log('Confidence: ' + e.results[0][0].confidence);

          socket.emit('chat message', text);
        });

        recognition.addEventListener('speechend', function() {
          recognition.stop();
        });

        recognition.addEventListener('error', function(e) {
          outputBot.textContent = 'Error: ' + e.error;
        });
    } 
    /*document.getElementById('go').addEventListener('click', function() {
      //alert($('#txtinput').val());
      var text = $('#txtinput').val();
      socket.emit('chat message', text);
    });*/


    function synthVoice(text) {
      const synth = window.speechSynthesis;
      const utterance = new SpeechSynthesisUtterance();
      utterance.text = text;
      synth.speak(utterance);
    }

    socket.on('bot reply', function(replyText) {
      //synthVoice(replyText);

      if(replyText == '') replyText = '(No answer...)';
      var msg = '<div class="row msg_container base_receive">';
      msg += '            <div class="avatar">';
      msg += '                   <img style="height:65px;width:65px;" src="../public/images/debbie avatar.png" class=" img-responsive ">';
      msg += '  </div> <div class="col-md-10 col-xs-10"> <div class="messages msg_receive"> <p>';
      msg += replyText;
      msg += '</p><time datetime="2009-11-13T20:00">AIMEE at ';
      msg += new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
      msg += '</time> </div> </div></div>';

      $('#imgprogress').remove();
      $('#converse').append(msg);
      $('#converse').animate({ "scrollTop": $('#converse')[0].scrollHeight }, "fast");
      //outputBot.textContent = replyText;
    });
    $('#txtmessage').bind("enterKey", function (e) {
      var text = $('#txtmessage').val();
      if(text && text != "")
      {
          //var luisurl = 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/8b293613-0625-47d5-9ba8-fe28e8828b85?subscription-key=80936422fc7342e690e8a72c53b992b9&verbose=true&timezoneOffset=0&spellCheck=true&q=';
          // Make LUIS call
          //$.get(luisurl+ text, function (result) {
          //  var response = result;
            // Post Message
            
           
          //});
          postmessage(text);

          if(text.indexOf("start over") != -1 || text.indexOf("startover") != -1 || text.indexOf("start fresh") != -1)
          {
            $('#converse').empty("");
            $('#converse').append('<div id="imgprogress"><img src="../public/images/inprogress.gif" width="50px" height="50px"  /></div>');
            $('#txtmessage').val("");
          }else{
            var appendhtml = '<div class="row msg_container base_sent"><div class="col-md-10 col-xs-10"><div class="messages msg_sent"><p>';
            appendhtml += text;
            appendhtml += '</p><time datetime="2009-11-13T20:00"> You at ';
            appendhtml += new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
            appendhtml += '</time></div></div><div class="avatar"><img style="height:50px;width:50px;border-radius:50%;" src="../public/images/img-john.png" class=" img-responsive "/></div></div>';
            $('#converse').append(appendhtml);
            $('#converse').append('<div id="imgprogress"><img src="../public/images/inprogress.gif" width="50px" height="50px"  /></div>');
            $('#converse').animate({ "scrollTop": $('#converse')[0].scrollHeight }, "fast");

            $('#txtmessage').val("");
          }
        }
      });
      $('#txtmessage').keyup(function (e) {
          if (e.keyCode == 13) {
              $(this).trigger("enterKey");
          }
      });


      function postmessage(msg) {
            var url = baseurl + '/post/' + msg;
            posted_msg = msg;
            console.log(url);
            $.get(url, function (result) {
                if (result && (result.data != "undefined")) {

                    var _msg = result.data;
                    var replace = "";
                    var _msg = result.data.replace("<p>" + posted_msg + "</p>", replace);
                    var _msg = _msg.replace("<p>" + posted_msg.substring(0, (posted_msg.length - 1)) + "</p>", replace);
                    //alert(_msg.indexOf(" I am Debbie"));
                    socket.emit('chat message', _msg);
                }
            });

        }
});


function getBrowser(){
    var browserName, nAgt, verOffset;
    nAgt = navigator.userAgent;
    // In Opera, the true version is after "Opera" or after "Version"
    if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
      browserName = "Opera";
    }
    // In MSIE, the true version is after "MSIE" in userAgent
    else if ((verOffset=nAgt.indexOf("Windows"))!=-1) {
      browserName = "Microsoft Internet Explorer";
    }
    // In Chrome, the true version is after "Chrome" 
    else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
      browserName = "Chrome";
    }
    // In Safari, the true version is after "Safari" or after "Version" 
    else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
      browserName = "Safari";
    }
    // In Firefox, the true version is after "Firefox" 
    else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
        browserName = "Firefox";
    }
    return browserName;
}
